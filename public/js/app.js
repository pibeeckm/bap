function RangedAverage(arr, r)
{
    x = Average(arr);
    //now eliminate items r% out of range
    for(var i=0; i<arr.length; i++)
        if(arr[i] < (x/r) || arr[i]>(x*(1+r)))
            arr.splice(i,1);
    x = Average(arr); //compute new average
    return x;
}
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
Date.prototype.format = function() {
   var yyyy = this.getFullYear().toString();
   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
   var dd  = this.getDate().toString();
    var hh = this.getHours().toString();
    var m = this.getMinutes().toString();
    return yyyy +'/'+ (mm[1]?mm:"0"+mm[0]) +'/'+  (dd[1]?dd:"0"+dd[0]) +' '+  (hh[1]?hh:"0"+hh[0]) + ':' + (m[1]?m:"0"+m[0]) ; // padding
  };
String.prototype.hashCode = function() {
  var hash = 0, i, chr, len;
  if (this.length == 0) return hash;
  for (i = 0, len = this.length; i < len; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

Number.prototype.toRad = function() {
  return this * Math.PI / 180;
}

var App = {
	init: function () {
		setTimeout(function () {
				var lastPart = window.location.href.split("/").pop();
		console.log(lastPart);
		switch (lastPart) {
			case 'control':
				$('.nav-tab.control')[0].click();
				console.log("control clicked");
				break;
			case 'metrics':
				$('.nav-tab.metrics')[0].click();
				console.log("metrics clicked");
				break;
			case 'settings':
				$('.nav-tab.settings')[0].click();
				console.log("settings clicked");
				break;
		}
		}, 1);
		
		$('.chart-wrapper').each(function () {
			var mac = $(this).data('mac');
			var metric = $(this).data('metric');
			
			var id = App.getId([mac, metric]);
			$(this).attr('id', id);
			App.getMetrics(mac, metric, id);
		});
		
		
		console.log(deviceArray);
		
		

		L.mapbox.accessToken = 'pk.eyJ1IjoiZmdtbnRzIiwiYSI6IjlmNmYxNjYwNDhjNDU0YjEyOTU1YmYxNjFmZTZiYWFkIn0.0xY76xZ0sYoh4cDrbBBZdA';
		App.map = L.mapbox.map('map', 'mapbox.streets')
			.setView([51.203630, 3.517075], 9);
		this.initPosition();
		this.bindEvents();
		this.initSocket();
		console.log("ready");
	},
	getId: function (arr) {
		return JSON.stringify(arr).hashCode();
	},
	initPosition: function () {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(App.GEOsuccess, App.GEOerror);
		} else {
			console.log("adres popup");
		}
	},
	GEOsuccess: function (position) {
		App.clientLat = position.coords.latitude;
		App.clientLon = position.coords.longitude;
		//App.loadMap([App.lat, App.lon]);
	},
	getMetrics: function (mac, metric, id) {
		console.log("getting metrics");
		App.api('metrics', { 'mac': mac, 'metric': metric }, id);
	},
	bindEvents: function () {
		$(document).on('click', '.btn-add-device', function () {
			$(".add-device-overlay").toggleClass('active');
		})
			.on('click', '.toggle-top', function () {
			$('.topbar').toggleClass("show");
		})
			.on('click', '.btn-save-device', function () {
			//JSON.stringify()
			var formData = $('#frm-add-device').serializeObject();
			console.log(formData);
			App.api('adddevice', formData);
			$('#frm-add-device')[0].reset();
		})
			.on('click', '.btn-share-device', function () {
			//JSON.stringify()
			var formData = $('#frm-share-device').serializeObject();
			console.log(formData);
			App.api('sharedevice', formData);
			$('#frm-share-device')[0].reset();
			$(".mui-overlay").hide();
			//$.snackbar({ content: 'Device shared with ' + formData.gmail, style: 'toast' });
			
			toastr["success"]("Device succesfully shared with"  + formData.gmail, "Share Device");
		})
			.on('click', '.mui-overlay .modal', function (e) {
			e.preventDefault();
			e.stopPropagation();
		})
			.on('click', '.mui-overlay', function (e) {
			$(this).toggleClass('active');
		})
			.on('submit', '#frm-add-device', function (e) {
			e.preventDefault();
			})
			.on('submit', '#frm-share-device', function (e) {
			e.preventDefault();
			})
			.on('click', '.nav-tab', function () {
			//location.hash = $(this).data("url");
			//function () {
					//	$(window).resize();
					//}
					
			if ($(this).data("url") == "metrics") {
				setTimeout(function () {
				$(window).resize();
				console.log("resize triggered");
				}, 300);
			}	else
			if ($(this).data("url") == "dashboard") {
				setTimeout(function () {
					App.map._onResize(); 
				
				console.log("resize triggered");
				}, 300);
			}
			
			//map._onResize(); 
					
			window.history.pushState("object or string", "title", "http://bap.fgmnts.be/" + $(this).data("url"));
		})
			.on('click', '.device .md-share', function () {
				// modal open en vragen om email en tijd
			$("#deviceid").val($(this).data('id'));
				$(".share-device-overlay").toggleClass('active');
		})
			.on('click', '.device .md-delete', function () {
				App.api('removedevice', { id: $(this).data('id') });
			})
			.on('change', '.switch', function () {
			console.log($(this).data('id'));
			console.log($(this).prop('checked'));
			var id = $(this).data('id');
			var mac = $(this).data('mac');
			//App.data[id] = $(this).prop('checked') ? 1 : 0;
			App.socket.emit('control', { 'id': $(this).data('id'), 'value': $(this).prop('checked') }, mac)
				
		});

	},
	 calculateDistance: function(lat1, lon1, lat2, lon2) {
		  var R = 6371000; // m
		  var dLat = (lat2 - lat1).toRad();
		  var dLon = (lon2 - lon1).toRad(); 
		  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		          Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
		          Math.sin(dLon / 2) * Math.sin(dLon / 2); 
		  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); 
		  var d = R * c;
		  if (d < 100)
			  $(".autocontrol-wrapper").show();
		  else
			  $(".autocontrol-wrapper").hide();
		  return d;
	},
	api: function (route, data, extra) {
		$.ajax({
			type: "POST",
			url: App.baseUrl + route,
			dataType: 'json',
			data: {
				data: JSON.stringify(data)
			},
			cache: false,
			beforeSend: function () {
			},
			success: function (response) {
				console.log(response);
				if (route == "metrics") {
					var data = [];
					RangedAverage(response,10);
					for (var a = 0; a < response.length; a++) {
						if (parseFloat(response[a].value) < 150)
							data.push([response[a].timestamp, parseFloat(response[a].value)]);
					}
					//chart-wrapper
					var title = $("#" + extra).prev('h3').text();
					$('#' + extra).highcharts({
						chart: {
							type: 'line',
            				zoomType: 'xy'
						},
						exporting: { enabled: false },
						title: {
							text: title
						},
						legend: {
							enabled: false
						},
						xAxis: {
							type: 'datetime',
							labels: {
								formatter: function () {
									return (new Date(this.value)).format();
								}
							}
						},
						yAxis: {
							title: {
								text: title
							},
						},
						series: [{
							data: data
						}],
						plotOptions: {
							line: {
								connectNulls: false
							}
						}
					});
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
			},
			complete: function (e) {
				setTimeout(function () {
					$(window).resize();
				}, 500); 
				
			}
		});
	},
	toDecimal: function (value) {
		var result = null;
        var degValue = value / 100;
        var degrees = parseInt(degValue);
        var decMinutesSeconds = ((degValue - degrees)) / .60;
        var minuteValue = decMinutesSeconds * 60;
        var minutes = parseInt(minuteValue);
        var secsValue = (minuteValue - minutes) * 60;
        result = degrees + "\u00B0" + " " + minutes + "' " + secsValue.toFixed(2) + "\" ";
		//console.log(result);
		return Math.sign(degrees) * (Math.abs(degrees) + (minutes / 60.0) + (secsValue / 3600.0));;
	},
	cv: function (input) {
		var output;
		//16384
		//(float)(ax-MPU6050_AXOFFSET)/MPU6050_AXGAIN
		output = input / 16384;
		return output.toFixed(2);	
	},
	cg: function (input) {
		var output;
		//16384
		//(float)(ax-MPU6050_AXOFFSET)/MPU6050_AXGAIN
		output = input / 16.4;
		return output.toFixed(2);	
	},
	draw: function () {
		var data = App.data;
		
		
		
		console.log(data);
		
		//console.log(App.cv(data.AcX) + " " + App.cv(data.AcY) + " " + App.cv(data.AcZ) + " " +
		//			App.cg(data.GyX) + " " + App.cg(data.GyY) + " " + App.cg(data.GyZ));
		
		//$('.back')
		
		var transforms = {
			'top': 'angleZ',
			'side': 'angleX',
			'back' : 'angleY'
		};
		
		$.each(transforms, function (id, value) {
			var div = $("#" + id + " .rotate-wrapper")[0];
			var deg = data[value];
			console.log(id + " " + deg);
			div.style.webkitTransform = 'rotate(' + deg + 'deg)';
						div.style.mozTransform = 'rotate(' + deg + 'deg)';
						div.style.msTransform = 'rotate(' + deg + 'deg)';
						div.style.oTransform = 'rotate(' + deg + 'deg)';
						div.style.transform = 'rotate(' + deg + 'deg)';
		});
		
		/*
		var div = document.getElementById("pitch"),
							deg = data.pitch;

						div.style.webkitTransform = 'rotate(' + deg + 'deg)';
						div.style.mozTransform = 'rotate(' + deg + 'deg)';
						div.style.msTransform = 'rotate(' + deg + 'deg)';
						div.style.oTransform = 'rotate(' + deg + 'deg)';
						div.style.transform = 'rotate(' + deg + 'deg)';
		div = document.getElementById("roll"),
							deg = data.roll;

						div.style.webkitTransform = 'rotate(' + deg + 'deg)';
						div.style.mozTransform = 'rotate(' + deg + 'deg)';
						div.style.msTransform = 'rotate(' + deg + 'deg)';
						div.style.oTransform = 'rotate(' + deg + 'deg)';
						div.style.transform = 'rotate(' + deg + 'deg)';
		*/
		
		var controls = ['stream', 'frontLight', 'backLight', 'halt'];
		for (var control in controls) {
			if (data.control != null) {
				$("#" + control).attr('checked', data.control);
			}
		}
		

		var lat = App.toDecimal(parseFloat(data.north));
		var lon = App.toDecimal(parseFloat(data.east));
		App.lat = lat;
		App.lon = lon;
		//console.log(lat);
		//console.log(lon);

		if (App.marker == null) {
			//console.log(data);

			App.marker = L.marker([lat, lon], {
				icon: L.divIcon({
					className: 'devicemarker',
					"iconSize": [26, 26], // size of the icon
					"iconAnchor": [13, 13]
				})
			});
			//App.marker.setLatLng(new L.LatLng());
			App.marker.addTo(App.map);
			App.map.setView([lat, lon], 13);

		}
		if (App.marker != null && App.clientMarker == null) {
			App.clientMarker = new L.marker([App.clientLat, App.clientLon], {
				icon: L.divIcon({
					className: 'marker',
					"iconSize": [26, 26], // size of the icon
					"iconAnchor": [13, 13]
				})
			});
			//App.marker.setLatLng(new L.LatLng());
			App.clientMarker.addTo(App.map);
			navigator.geolocation.watchPosition(function (position) {
				App.clientLat = position.coords.latitude;
				App.clientLon = position.coords.longitude;
			});
		}
		
		
		
		var newLatLng = new L.LatLng(lat, lon);
		App.marker.setLatLng(newLatLng);
		
		newLatLng = new L.LatLng(App.clientLat, App.clientLon);
		App.clientMarker.setLatLng(newLatLng);
		
		App.clientDistance = App.calculateDistance(lat, lon, App.clientLat, App.clientLon).toFixed(2);

		if ($("#auto").prop('checked')) {

			var mac = $("#auto").data('mac');
			var id = $("#auto").data('id');
			var value;
			if (parseInt(App.clientDistance) < 50) {
			// auto aan dus binnen 50 meter is start false
				value = false;
			} else {
				
				value = true;
			}
			if (App.data.halt != value)
			App.socket.emit('control', { 'id': id, 'value': value }, mac);
			
		} 
		
		
		App.map.setView([lat, lon]);
		$('.distance').text(App.clientDistance + "m");
		$('.speed').text(parseFloat(1.85200 * data.knots).toFixed(2));
		$('.altitude').text(parseFloat(data.altitude).toFixed(2));
		$('.temp').text(parseFloat(data.temperature).toFixed(1));
		$('.rpm').text(parseFloat(data.RPM).toFixed(0));
	},
	initSocket: function () {
		App.socket = io('http://dev.fgmnts.be');
		App.socket.on('connect', function () { });
		var user = "Unknown user";
		App.socket.emit('client-connect', user);
		console.log(deviceArray);

		for (var b = 0; b < deviceArray.length; b++) {
			var mac = deviceArray[b];
			var cmd = 'update-' + mac.replace(/:/g, '');
			console.log(cmd);
			App.socket.on(cmd, function (data) {
				App.data = data;
				$('.status.'+mac.replace(/:/g, '')).removeClass("red").addClass("green");
				App.draw();
			});
		}


		App.socket.on('update', function (data) {
			console.log("Data: " + data);
			var json = data;
			try {
				obj = JSON.parse(json);
				for (var key in obj) {
					//             console.log(key);
					if (key == "r" || key == "p" || key == "y") {
						var div = document.getElementById(key),
							deg = obj[key];

						div.style.webkitTransform = 'rotate(' + deg + 'deg)';
						div.style.mozTransform = 'rotate(' + deg + 'deg)';
						div.style.msTransform = 'rotate(' + deg + 'deg)';
						div.style.oTransform = 'rotate(' + deg + 'deg)';
						div.style.transform = 'rotate(' + deg + 'deg)';
					}
				}
			} catch (e) {
				obj = "error";
			}
		});

	},
	initHighcharts: function () {

	},
	socket: null,
	data: null,
	baseUrl: '/api/',
	marker: null,
	lat: null,
	lon: 0,
	clientLat: null,
	clientLon: null,
	clientMarker: null,
	clientDistance: null
};
(function () {
	App.init();
})();