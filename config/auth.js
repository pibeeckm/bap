// config/auth.js

// expose our config directly to our application using module.exports
module.exports = {

	'facebookAuth' : {
		'clientID' 		: 'your-secret-clientID-here', // your App ID
		'clientSecret' 	: 'your-client-secret-here', // your App Secret
		'callbackURL' 	: 'http://localhost:8080/auth/facebook/callback'
	},

	'twitterAuth' : {
		'consumerKey' 		: 'your-consumer-key-here',
		'consumerSecret' 	: 'your-client-secret-here',
		'callbackURL' 		: 'http://localhost:8080/auth/twitter/callback'
	},

	'googleAuth' : {
		'clientID' 		: '209380026509-2nov30ak3iha6c92dpeol1flbr2bjpss.apps.googleusercontent.com',
		'clientSecret' 	: 'JB7_myZ003Tm_5XS9AmNn-6O',
		'callbackURL' 	: 'http://bap.fgmnts.be/auth/google/callback'
	}

};
