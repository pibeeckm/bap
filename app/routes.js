module.exports = function (app, passport) {
	// models =====================================================================

var Device       = require('../app/models/device');
var User       = require('../app/models/user');
var UserHasDevice = require('../app/models/userHasDevice');


var MetricValue = require('../app/models/metric');
var Metrics = require('../app/models/metrics');



// normal routes ===============================================================

	// show the home page (will also have our login links)
	app.get('/nouser', function(req, res) {
		res.render('login.ejs');
});
	
// TEST SECTION =========================

	app.get('/:var(control|metrics|settings)?', isLoggedIn, function (req, res) {
		console.log('UserId: ' + req.user._id);
		UserHasDevice.find({'userid': req.user._id},  function(err, docs) {
		    if (!err){ 
				console.log(docs);
				var devicdeids = [];
				var ttl = {};
				for (var a = 0; a < docs.length; a++) {
					console.log(docs[a].ttl);
					if (docs[a].ttl == "true" || Date.now() <= docs[a].ttl) {
						
						devicdeids.push(docs[a].deviceid);
						ttl[docs[a].deviceid] = docs[a].ttl;
					}
				}
				console.log(devicdeids);
				console.log(ttl);
				Device.find()
				  .where('_id')
				  .in(devicdeids)
					.exec(function (err, records) {
					if (err)
						console.log(err);
						
					var devices = [];
					
					for (var a = 0; a < records.length; a++) {
				console.log(records[a]._id);
				console.log(ttl[records[a]._id]);
						records[a].ttl = ttl[records[a]._id];
						if (records[a].name != "" && records[a].mac != "")
							devices.push(records[a]);
						
						console.log(">>>>>>> Name: " + records[a].name);
					}
						
					res.render('index.ejs', {
						user : req.user,
				        devices : devices 
                	});
				  });
			} else { 
				console.log(err);
			 }
		});
});	
	
	
	
	
	
	
	
// API routes ===============================================================

	app.post('/api/metrics', function (req, res) {
		var data = JSON.parse(req.body.data);
		var mac = data.mac;
		var metric = data.metric;
		
		console.log("Api voor metrics: " + metric + " en mac: " + mac);
		
		Metrics.findOne({ 'name': metric }, function(err, parent) {
			if (err)
				console.log(err);
			if (parent != null) {
				var id = parent._id;
				MetricValue.find({ 'metricid': id, 'mac': mac }, function (err, values) {
					if (!err)
					res.end(JSON.stringify(values));
				});
			}
				
		});
	});
	
	app.post('/api/removedevice', function (req, res) {
		var data = JSON.parse(req.body.data);
		var deviceid = data.id;
		Device.findOne({ '_id': deviceid }, function (err, device) {
			if (err) return handleError(err);
			device.remove();
			UserHasDevice.findOne({ 'deviceid': deviceid, 'userid': req.user._id }, function (err, userHasDevice) {
				if (err) return handleError(err);
				userHasDevice.remove();
				var response = {
					status: 200,
					success: 'Removed Successfully'
				};

				res.end(JSON.stringify(response));
			});
		});
	});
	app.post('/api/sharedevice', function (req, res) {
		var data = JSON.parse(req.body.data);
		User.count({'google.email': data.gmail}, function (err, c){
			if (err)
			console.log(err);
			if (c == 1) {
				User.findOne({'google.email': data.gmail}, function (err, user){
					if (err)
					console.log(err);
					
					var ttl = data.ttl;
					console.log(ttl);
					
					var unit = ttl[ttl.length - 1];
					ttl = ttl.substring(0, ttl.length - 1);
					
					console.log(ttl);
					
					console.log(unit);
					switch (unit) {
						case 'm':
							ttl = ttl * 1000 * 60;
							break;
						case 's':
							ttl = ttl * 1000;
							break;
						case 'h':
							ttl = ttl * 1000 * 60 * 60;
							break;
						case 'd':
							ttl = ttl * 1000 * 60 * 60 * 24;
							break;
						case 'w':
							ttl = ttl * 1000 * 60 * 60 * 24 * 7;
							break;
					}
					
					console.log(ttl);
					ttl += Date.now();
					
					console.log(ttl);
					var userHasDevice = new UserHasDevice({
						userid: user._id,
						deviceid: data.deviceid,
						ttl: ttl
						});
					userHasDevice.save(function (err, data) {
							if (err) {
								console.log(err);
								var message = "Er ging iets mis 0x32";
								
				var response = {
					status: 200,
					success: message
				};

				res.end(JSON.stringify(response));
							} else {
								console.log(data);
								message = "Succes";
								console.log("0x34");
								
									var response = {
										status: 200,
										success: 'Shared Successfully'
									};
					
									res.end(JSON.stringify(response));
							}
						});
					
				})
			} else {
				
				var response = {
					status: 200,
					success: 'User doesn\'t exist ' + c
				};

				res.end(JSON.stringify(response));
			}
		})
		
		
	});
	app.post('/api/adddevice', function (req, res) {
		var data = JSON.parse(req.body.data);
		
		
		console.log(data.mac);
		console.log(data.mac);
		
		
		
		
		
		
		
		Device.findOne({ 'mac':  data.mac }, function (err, device) {
		  if (err) return handleError(err);
		  console.log(device) // Space Ghost is a talk show host.
		})
				
		
		
		
		
		
		var message;
		Device.count({ 'mac': data.mac }, function (err, c) {
			if (err)
				console.log(err);
				console.log(c);
			if (c == 0) {
			  
				console.log("0x01");
			  var device = new Device({ name: data.name,mac: data.mac });
			  device.save(function (err, data) {
  				if (err) {
					console.log(err);
			  		message = "Er ging iets mis 0x01";
				} else {
							console.log(data);
				console.log("0x03");
					var userHasDevice = new UserHasDevice({
			          userid : req.user._id,
			          deviceid : device._id,
			          ttl : true
						});
					userHasDevice.save(function (err, data) {
						if (err) {
							console.log(err);
							message = "Er ging iets mis 0x02";
						} else {
							console.log(data);
							message = "Succes";
							console.log("0x04");
						}
					});
				}
			});
		  } else {
			  message = "Apparaat reeds geregistreerd";
				console.log("0x02");
		  }
        });
		
		
		
		var response = {
		    status  : 200,
		    success : 'Updated Successfully'
		}
		
		res.end(JSON.stringify(response));
    });


	// PROFILE SECTION =========================
	app.get('/profile', isLoggedIn, function(req, res) {
		res.render('profile.ejs', {
			user : req.user
		});
	});



	// PROFILE SECTION =========================
	app.get('/devices', isLoggedIn, function(req, res) {
		Device.find({}, function(err, devices) {
			var deviceMap = {};

    			devices.forEach(function(device) {
    			  deviceMap[device._id] = device;
    			});
                	res.render('devices.ejs', {
				devices : devices
			});
		});
	});


	// SECTION =========================
	app.get('/add', isLoggedIn, function(req, res) {
		res.render('add.ejs');
	});


	// SECTION =========================
	app.get('/cpu', isLoggedIn, function(req, res) {
		res.render('cpu.ejs');
	});

    // ADD MAC TO USER
    // assuming POST: name=foo&color=red            <-- URL encoding
    //
    // or       POST: {"name":"foo","color":"red"}  <-- JSON encoding
    app.post('/add', isLoggedIn, function(req, res) {
        var count = 0;
        Device.count({ mac : req.body.mac }, function (err, c) {
          count = c;
        });
        if (count == 0) {  
        var device = new Device({
          name : req.body.name,
          mac : req.body.mac
        });  
        var userHasDevice = new userHasDevice({
          userid : req.user._id,
          deviceid : device._id,
          userId : true
        });
		//userHasDevice
		
        device.save(function (err, data) {
          if (err) {
			console.log(err);
			} else {
			device.save(function (err, data) {
			  if (err) console.log(err);
			  else console.log('Saved : ', data );
			});
			}
			});
        } else {
			var device = new Device({
			  name : 'Bestaat al',
			  mac : req.body.mac
			}); 
        }
        res.render('test.ejs', {
            user : req.user,
            msg : "Device created: " + device.name
        });
    });

	
	// LOGOUT ==============================
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/nouser');
	});

// =============================================================================
// AUTHENTICATE (FIRST LOGIN) ==================================================
// =============================================================================

	// locally --------------------------------
		// LOGIN ===============================
		// show the login form
		app.get('/login', function(req, res) {
			res.render('login.ejs', { message: req.flash('loginMessage') });
		});

		// process the login form
		app.post('/login', passport.authenticate('local-login', {
			successRedirect : '/', // redirect to the secure profile section
			failureRedirect : '/login', // redirect back to the signup page if there is an error
			failureFlash : true // allow flash messages
		}));

		// SIGNUP =================================
		// show the signup form
		app.get('/signup', function(req, res) {
			res.render('signup.ejs', { message: req.flash('signupMessage') });
		});

		// process the signup form
		app.post('/signup', passport.authenticate('local-signup', {
			successRedirect : '/', // redirect to the secure profile section
			failureRedirect : '/signup', // redirect back to the signup page if there is an error
			failureFlash : true // allow flash messages
		}));

	// facebook -------------------------------

		// send to facebook to do the authentication
		app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

		// handle the callback after facebook has authenticated the user
		app.get('/auth/facebook/callback',
			passport.authenticate('facebook', {
				successRedirect : '/',
				failureRedirect : '/nouser'
			}));

	// twitter --------------------------------

		// send to twitter to do the authentication
		app.get('/auth/twitter', passport.authenticate('twitter', { scope : 'email' }));

		// handle the callback after twitter has authenticated the user
		app.get('/auth/twitter/callback',
			passport.authenticate('twitter', {
				successRedirect : '/',
				failureRedirect : '/nouser'
			}));


	// google ---------------------------------

		// send to google to do the authentication
		app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

		// the callback after google has authenticated the user
		app.get('/auth/google/callback',
			passport.authenticate('google', {
				successRedirect : '/',
				failureRedirect : '/nouser'
			}));

// =============================================================================
// AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
// =============================================================================

	// locally --------------------------------
		app.get('/connect/local', function(req, res) {
			res.render('connect-local.ejs', { message: req.flash('loginMessage') });
		});
		app.post('/connect/local', passport.authenticate('local-signup', {
			successRedirect : '/', // redirect to the secure profile section
			failureRedirect : '/connect/local', // redirect back to the signup page if there is an error
			failureFlash : true // allow flash messages
		}));

	// facebook -------------------------------

		// send to facebook to do the authentication
		app.get('/connect/facebook', passport.authorize('facebook', { scope : 'email' }));

		// handle the callback after facebook has authorized the user
		app.get('/connect/facebook/callback',
			passport.authorize('facebook', {
				successRedirect : '/',
				failureRedirect : '/nouser'
			}));

	// twitter --------------------------------

		// send to twitter to do the authentication
		app.get('/connect/twitter', passport.authorize('twitter', { scope : 'email' }));

		// handle the callback after twitter has authorized the user
		app.get('/connect/twitter/callback',
			passport.authorize('twitter', {
				successRedirect : '/',
				failureRedirect : '/nouser'
			}));


	// google ---------------------------------

		// send to google to do the authentication
		app.get('/connect/google', passport.authorize('google', { scope : ['profile', 'email'] }));

		// the callback after google has authorized the user
		app.get('/connect/google/callback',
			passport.authorize('google', {
				successRedirect : '/',
				failureRedirect : '/nouser'
			}));

// =============================================================================
// UNLINK ACCOUNTS =============================================================
// =============================================================================
// used to unlink accounts. for social accounts, just remove the token
// for local account, remove email and password
// user account will stay active in case they want to reconnect in the future

	// local -----------------------------------
	app.get('/unlink/local', isLoggedIn, function(req, res) {
		var user            = req.user;
		user.local.email    = undefined;
		user.local.password = undefined;
		user.save(function(err) {
			res.redirect('/profile');
		});
	});

	// facebook -------------------------------
	app.get('/unlink/facebook', isLoggedIn, function(req, res) {
		var user            = req.user;
		user.facebook.token = undefined;
		user.save(function(err) {
			res.redirect('/profile');
		});
	});

	// twitter --------------------------------
	app.get('/unlink/twitter', isLoggedIn, function(req, res) {
		var user           = req.user;
		user.twitter.token = undefined;
		user.save(function(err) {
			res.redirect('/profile');
		});
	});

	// google ---------------------------------
	app.get('/unlink/google', isLoggedIn, function(req, res) {
		var user          = req.user;
		user.google.token = undefined;
		user.save(function(err) {
			res.redirect('/profile');
		});
	});


};

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
	if (req.isAuthenticated())
		return next();

	res.redirect('/nouser');
}
