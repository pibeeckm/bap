// load the things we need
var mongoose = require('mongoose');
//var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var metricSchema = mongoose.Schema({
    metricid     : String,
    value: String,
    mac: String,
    timestamp : String
});

// create the model for users and expose it to our app
module.exports = mongoose.model('MetricValue', metricSchema);
