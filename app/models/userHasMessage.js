// load the things we need
var mongoose = require('mongoose');
//var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var userHasMessage = mongoose.Schema({
    userid       : String,
    messageid     : String,
	read			: Boolean (?)
});

// create the model for users and expose it to our app
module.exports = mongoose.model('UserHasMessage', userHasMessage);
