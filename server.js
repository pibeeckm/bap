// server.js

// set up ======================================================================
// get all the tools we need
var express  = require('express');
var app      = express();
var port     = 8030; //process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');

//var http = require('http').Server(app);
//var io = require('socket.io')(app);

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session = require('express-session');

var async = require('async');


var configDB = require('./config/database.js');

// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

app.use(express.static(process.cwd() + '/public'));






// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport






var Device       = require('./app/models/device');
var User       = require('./app/models/user');
var UserHasDevice = require('./app/models/userHasDevice');
var MetricValue = require('./app/models/metric');
var Metrics = require('./app/models/metrics');




// socket.io ===================================================================

var http = require('http').Server(app);
var io = require('socket.io')(http);



var mac = null;
var tmpStatus = "";

io.on('connection', function (socket) {
  
  
  socket.on('sync', function (date, metrics, mac) {
    var cmd = 'update-' + mac.replace(/:/g, '');
    io.emit(cmd, metrics);
    

    async.forEachOf(metrics, function (value, name, callback) {
      
      Metrics.count({ 'name': name }, function (err, c) {
        if (err)
  
            
        if (c == 0) {
          //Make new metric
          var metric = new Metrics({ 'name': name });
          metric.save(function (err, data) {
            if (err) {
              console.log(err);
              //message = "Er ging iets mis 0x11";
            } else {
              
              var metricvalue = new MetricValue({ 'metricid': metric._id, value: value, 'mac': mac, timestamp: date });
              metricvalue.save(function (err, data) {
                if (err)
                  console.log(err);

                //console.log(data);
              });
              
              
              console.log(data);
            }
          });
        } else {
          Metrics.findOne({ 'name': name }, function (err, metric) {
            if (err)
              console.log(err);
            else {
              var metricvalue = new MetricValue({ 'metricid': metric._id, value: value, 'mac': mac, timestamp: date });
              metricvalue.save(function (err, data) {
                if (err)
                  console.log(err);
                //console.log(data);
              });
            }  
              
          });
        }
      
      });
    }, function (error) {
        if (error) console.log(error);

});
  });
  
  
// =============================================================================
  
  
  console.log('a user connected');
  if (tmpStatus != "") {
    io.emit('led status', tmpStatus);
  }
  socket.on('iot-connect', function(data) {
    console.log('a thing connected');
    console.log(data);
  });
  socket.on('serial open', function(data) {
    console.log('Open serial connection on ' + mac);
  });
  
  
  
  
  
  
  socket.on('control', function (data, mac) {
    console.log(mac);
    console.log(data);
    var i;
      var cmd = 'control-' + mac.replace(/:/g, '');
    switch (data.id) {
      case 'auto':
      //special case
        break;
      case 'stream':
        i = data.value ? 1 : 2;
        break;
      case 'frontLight':
        i = data.value ? 3 : 4;
        break;
      case 'backLight':
        i = data.value ? 5 : 6;
        break;
      case 'halt':
        i = data.value ? 10 : 9;
        break;
        
    }
    console.log(i);
    
    io.emit(cmd, i);
    console.log("emitted " + cmd);
  });
  
  
  socket.on('client-connect', function(client) {
   console.log('client ' + client + ' connected');
  });
});


// launch ======================================================================

http.listen(port, function(){
  console.log('listening on *:'+port);
});

